package com.payconiq.entity.dtos;

import java.math.BigDecimal;
import java.util.Date;

public class StockDTO {
	
	private Integer Id;
	private String name;
	private BigDecimal currentPrice;
	private Date lastUpdated;
	
	
	
	
	@Override
	public String toString() {
		return "StockDTO [Id=" + Id + ", name=" + name + ", currentPrice=" + currentPrice + ", lastUpdated="
				+ lastUpdated + "]";
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getCurrentPrice() {
		return currentPrice;
	}
	public void setCurrentPrice(BigDecimal currentPrice) {
		this.currentPrice = currentPrice;
	}
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	
	
	

}
