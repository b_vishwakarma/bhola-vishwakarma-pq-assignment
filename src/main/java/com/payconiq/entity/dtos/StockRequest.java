package com.payconiq.entity.dtos;

import java.math.BigDecimal;

public class StockRequest {
	
	private String name;
	
	private BigDecimal currentPrice;
	
	
	
	
	@Override
	public String toString() {
		return "StockRequest [name=" + name + ", currentPrice=" + currentPrice + "]";
	}
	public BigDecimal getCurrentPrice() {
		return currentPrice;
	}
	public void setCurrentPrice(BigDecimal currentPrice) {
		this.currentPrice = currentPrice;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	 

}
