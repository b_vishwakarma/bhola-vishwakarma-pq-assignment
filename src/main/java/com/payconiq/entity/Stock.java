package com.payconiq.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@DynamicUpdate
public class Stock implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1946669768933748041L;

	@javax.persistence.Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Integer Id;
	
	@Version	
	private int versionId;
	
	@Column(nullable = false,unique = true, insertable = true , updatable = true, length = 200)
	@NotBlank(message = "Stock name cannot be blank")	
    @Size(min=1, max=200 , message = "Stock name lenght should be between 1 to 200 characters")
	private String name;
	
	@Column(scale=2)
	@NotNull(message = "Stock price cannot be blank")
	private BigDecimal currentPrice;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")	
	private Date lastUpdated;
	
	public Stock() {
		super();
	}

	public Stock(
			@NotBlank(message = "Stock name cannot be blank") @Size(min = 1, max = 200, message = "Stock name lenght should be between 1 to 200 characters") String name,
			@NotNull(message = "Stock price cannot be blank") BigDecimal currentPrice) {
		super();
		this.name = name;
		this.currentPrice = currentPrice;
	}

	@PreUpdate
	public void postPersist() {
		lastUpdated = new Date();
	}
	
	@PrePersist
	public void prePersist() {
		Id = null;
		lastUpdated = new Date();
	}	
	
	
	public Integer getId() {
		return this.Id;
	}

	public void setId(Integer id) {
		this.Id = id;
	}

	public int getVersionId() {
		return versionId;
	}

	public void setVersionId(int versionId) {
		this.versionId = versionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(BigDecimal currentPrice) {
		this.currentPrice = currentPrice;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	

	
	
	
	
	
	
	

}
