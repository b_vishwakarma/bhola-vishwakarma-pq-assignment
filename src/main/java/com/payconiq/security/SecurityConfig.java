package com.payconiq.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	/*
	 * We need to disable csrf as Spring Security by default enables it. 
	 * So POST , PUT , DELETE wont work from swagger.  
	 * With the below code change this will bypass all the swagger request for csrf token
	 * */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/swagger-ui/**").permitAll().anyRequest().authenticated().and()
		.csrf().disable().httpBasic();
	}

}
