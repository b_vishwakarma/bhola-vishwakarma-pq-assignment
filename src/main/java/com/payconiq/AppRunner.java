package com.payconiq;

import java.math.BigDecimal;

import javax.annotation.PostConstruct;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.payconiq.entity.Stock;
import com.payconiq.repository.IStockRepository;

@SpringBootApplication
public class AppRunner {
	@Autowired
	IStockRepository iStockRepository;

	public static void main(String[] args) {
		SpringApplication.run(AppRunner.class, args);

	}

	@PostConstruct
	public void init() {
		iStockRepository.save(new Stock("Apple", new BigDecimal(20.5)));
		iStockRepository.save(new Stock("Google", new BigDecimal(50.5)));
		iStockRepository.save(new Stock("Tesla", new BigDecimal(92.5)));
		iStockRepository.save(new Stock("Jp Morgan", new BigDecimal(41)));
		iStockRepository.save(new Stock("Bhel", new BigDecimal(89.5)));
		iStockRepository.save(new Stock("RIL", new BigDecimal(24.5)));
		iStockRepository.save(new Stock("Ongc", new BigDecimal(53.5)));
		iStockRepository.save(new Stock("BNY", new BigDecimal(93.5)));
		iStockRepository.save(new Stock("Calstone", new BigDecimal(31)));
		iStockRepository.save(new Stock("Mcdonals", new BigDecimal(29.5)));

		iStockRepository.save(new Stock("STS", new BigDecimal(20.5)));
		iStockRepository.save(new Stock("MSIL", new BigDecimal(50.5)));
		iStockRepository.save(new Stock("TTP", new BigDecimal(92.5)));
		iStockRepository.save(new Stock("Jira", new BigDecimal(41)));
		iStockRepository.save(new Stock("Epam", new BigDecimal(89.5)));
		iStockRepository.save(new Stock("Cloud", new BigDecimal(24.5)));
		iStockRepository.save(new Stock("Energy", new BigDecimal(53.5)));
		iStockRepository.save(new Stock("Infra", new BigDecimal(93.5)));
		iStockRepository.save(new Stock("HDFC", new BigDecimal(31)));
		iStockRepository.save(new Stock("ICICI", new BigDecimal(29.5)));
	}
	
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

}
