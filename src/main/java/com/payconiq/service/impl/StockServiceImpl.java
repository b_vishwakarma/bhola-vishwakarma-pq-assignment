package com.payconiq.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.payconiq.entity.Stock;
import com.payconiq.entity.dtos.StockDTO;
import com.payconiq.exception.InvalidInputException;
import com.payconiq.exception.RecordNotFoundException;
import com.payconiq.exception.StockAlreadyPresentException;
import com.payconiq.repository.IStockRepository;
import com.payconiq.service.IStockService;

@Service
public class StockServiceImpl implements IStockService {

	Logger logger = LoggerFactory.getLogger(StockServiceImpl.class);

	@Autowired
	IStockRepository stockRepository;

	@Value("${default.page.size}")
	public Integer defaultPageSize;

	public Integer defaultPageNo = 0;

	@Autowired
	ModelMapper modelMapper;

	@Override
	public List<StockDTO> getStocks(Integer pageNo, Integer size)
			throws RecordNotFoundException, InvalidInputException {
		List<StockDTO> stockDTOs = new LinkedList<>();
		pageNo = ObjectUtils.defaultIfNull(pageNo, 0) == 0 ? 0 : pageNo - 1;
		size = ObjectUtils.defaultIfNull(size, 0) == 0 ? defaultPageSize : size;

		if (pageNo < 0 || size < 0)
			throw new InvalidInputException("Page Number or Page Size should not be less then zero");

		logger.info("Check Page No {} , Page Size {}", pageNo, size);

		Pageable pageable = PageRequest.of(pageNo, size, Sort.by("name"));
		List<Stock> stocks = stockRepository.findAll(pageable).getContent();
		logger.info("Result set size :: {}", stocks.size());
		if (!stocks.isEmpty()) {
			stocks.stream().forEach(s -> {
				stockDTOs.add(modelMapper.map(s, StockDTO.class));
			});
			return stockDTOs;
		}

		throw new RecordNotFoundException("No stocks are present. Try adjusting page number");
	}

	@Override
	public StockDTO createStock(StockDTO stockDto) throws StockAlreadyPresentException {
		Optional<Stock> st = stockRepository.findByName(stockDto.getName());
		if (st.isEmpty()) {
			Stock s = modelMapper.map(stockDto, Stock.class);
			stockRepository.save(s);
			logger.info("New stock created with Id :: {}", s.getId());
			return modelMapper.map(s, StockDTO.class);
		}

		throw new StockAlreadyPresentException("Stock : " + stockDto.getName() + " : already present.");

	}

	@Override
	public StockDTO getStockById(int Id) throws RecordNotFoundException {
		Optional<Stock> s = stockRepository.findById(Id);
		if (s.isPresent())
			return modelMapper.map(s.get(), StockDTO.class);

		throw new RecordNotFoundException("No stock present for the given ID : " + Id);
	}

	@Override
	public StockDTO updateStock(int Id, StockDTO stockDto) throws RecordNotFoundException {
		Optional<Stock> stock = stockRepository.findById(Id);
		if (stock.isPresent()) {
			logger.info(" Stock is present for Id :: {} ", String.valueOf(Id));
			Stock st = stock.get();
			if (!StringUtils.isBlank(stockDto.getName()))
				st.setName(stockDto.getName().equals(st.getName()) ? st.getName() : stockDto.getName());

			if (stockDto.getCurrentPrice() != null)
				st.setCurrentPrice(stockDto.getCurrentPrice() == st.getCurrentPrice() ? st.getCurrentPrice()
						: stockDto.getCurrentPrice());

			stockRepository.save(st);
			logger.info(" Stock updated for Id :: {}", String.valueOf(Id));
			return modelMapper.map(stock.get(), StockDTO.class);
		}
		logger.info(" Stock not present for Id :: {} ", String.valueOf(Id));
		throw new RecordNotFoundException("No stock present for the given ID : " + Id);
	}

	@Override
	public void deleteStock(int Id) throws RecordNotFoundException {
		Optional<Stock> s = stockRepository.findById(Id);
		if (s.isPresent())
			stockRepository.deleteById(Id);
		else
			throw new RecordNotFoundException("Not a valid stock Id : " + Id + " to be deleted");
	}

}
