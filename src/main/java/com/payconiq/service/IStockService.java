package com.payconiq.service;

import java.util.List;

import com.payconiq.entity.dtos.StockDTO;
import com.payconiq.exception.InvalidInputException;
import com.payconiq.exception.RecordNotFoundException;
import com.payconiq.exception.StockAlreadyPresentException;

public interface IStockService {	
	
	public List<StockDTO> getStocks(Integer pageNo , Integer size) throws RecordNotFoundException,InvalidInputException;
	public StockDTO getStockById(int Id) throws RecordNotFoundException;	
	public StockDTO createStock(StockDTO stock) throws StockAlreadyPresentException;
	public StockDTO updateStock(int id ,StockDTO stock)  throws RecordNotFoundException;	
	public void deleteStock(int id) throws RecordNotFoundException;

}
