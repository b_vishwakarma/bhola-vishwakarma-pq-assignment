package com.payconiq.exception;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class AppExceptionHandler extends ResponseEntityExceptionHandler {
	Logger logger = LoggerFactory.getLogger(AppExceptionHandler.class);

	@ExceptionHandler(value = RecordNotFoundException.class)
	public ResponseEntity<Object> recordNotFoundException(RecordNotFoundException exception) {
		List<String> errors = new ArrayList<String>();
		errors.add(exception.getMessage());
		ErrorDTO errorDto = new ErrorDTO(HttpStatus.NOT_FOUND.value(), new Date(), errors);
		logger.error(errorDto.toString());
		return new ResponseEntity<>(errorDto, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler({StockAlreadyPresentException.class,InvalidInputException.class})
	public ResponseEntity<Object> stockAlreadyPresentException(Exception exception) {
		List<String> errors = new ArrayList<String>();
		errors.add(exception.getMessage());
		ErrorDTO errorDto = new ErrorDTO(HttpStatus.BAD_REQUEST.value(), new Date(), errors);
		logger.error(errorDto.toString());
		return new ResponseEntity<>(errorDto, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler({
		ConstraintViolationException.class		
	 })
	protected ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException cve) {
		List<String> errors = new ArrayList<String>();
		ErrorDTO errorDto = new ErrorDTO(HttpStatus.BAD_REQUEST.value(), new Date(), errors);
		cve.getConstraintViolations().stream().forEach(c -> {
			errors.add(c.getMessage());
		});	    
		logger.error(errorDto.toString());
	    return new ResponseEntity<>(errorDto, HttpStatus.BAD_REQUEST);
	  }
	
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		List<String> errors = new ArrayList<String>();
		if (ex != null) {
			errors = ex.getBindingResult().getFieldErrors().stream().map(x -> x.getDefaultMessage())
					.collect(Collectors.toList());
		}
		ErrorDTO errorDto = new ErrorDTO(status.value(), new Date(), errors);		
		logger.error(errorDto.toString());
		return new ResponseEntity<>(errorDto, headers, status);

	}
	

}
