package com.payconiq.exception;

public class RecordNotFoundException extends Exception implements Cloneable{
	
	public RecordNotFoundException (String str)  
    {  
        super(str);  
    }
}
