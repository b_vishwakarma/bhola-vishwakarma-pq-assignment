package com.payconiq.exception;

public class StockAlreadyPresentException extends Exception {

	public StockAlreadyPresentException (String str)  
    {  
        super(str);  
    }
}
