package com.payconiq.exception;

import java.util.Date;
import java.util.List;

public class ErrorDTO {
	
	public Integer status;
	public Date timestamp;
	public List<String> errors;
	
	public ErrorDTO(Integer status, Date timestamp, List<String> errors) {
		super();
		this.status = status;
		this.timestamp = timestamp;
		this.errors = errors;
	}

	@Override
	public String toString() {
		return "ErrorDTO [status=" + status + ", timestamp=" + timestamp + ", errors=" + errors + "]";
	}
	
	
	
	

}
