package com.payconiq.exception;

public class InvalidInputException extends Exception {

	
	public InvalidInputException (String str)  
    {  
        super(str);  
    }
}
