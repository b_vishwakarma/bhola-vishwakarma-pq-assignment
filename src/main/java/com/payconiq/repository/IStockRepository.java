package com.payconiq.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.payconiq.entity.Stock;

@Repository
public interface IStockRepository extends JpaRepository<Stock,Integer>,PagingAndSortingRepository<Stock, Integer>	{
	
	public Optional<Stock> findByName(String name);
	
	public Page<Stock> findAll(Pageable pageable);
	
	@Query(value = "SELECT max(id) from Stock")
	int getMaxStockId();

}
