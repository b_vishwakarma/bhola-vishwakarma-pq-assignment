package com.payconiq.controller;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.payconiq.entity.dtos.StockDTO;
import com.payconiq.entity.dtos.StockRequest;
import com.payconiq.exception.InvalidInputException;
import com.payconiq.exception.RecordNotFoundException;
import com.payconiq.exception.StockAlreadyPresentException;
import com.payconiq.service.IStockService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RestController
@RequestMapping(value = "/api/")
public class StockController {
	Logger logger = LoggerFactory.getLogger(StockController.class);

	@Autowired
	IStockService iStockService;

	@Autowired
	ModelMapper modelMapper;

	
	@Operation(summary = "Get stocks on page number with page display size. Stocks are sorted by name in ascending order")
	@ApiResponses(value = { 
	  @ApiResponse(responseCode = "200", description = "Stocks are present", 
	    content = { @Content(mediaType = "application/json", 
	      schema = @Schema(implementation = StockDTO.class)) }),
	  @ApiResponse(responseCode = "400", description = "Bad request", 
	    content = @Content), 
	  @ApiResponse(responseCode = "404", description = "Stock not present", 
	    content = @Content) })
	@GetMapping(value = "v1/stocks",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<StockDTO>> getAllStocks(
			@Valid @RequestParam(required = false, value = "pageNo") Integer pageNo,
			@Valid @RequestParam(required = false, value = "size") Integer size) throws RecordNotFoundException , InvalidInputException {
		logger.info("GET :: v1/stocks?pageNo={}&size={}", pageNo, size);
		List<StockDTO> stocks = iStockService.getStocks(pageNo, size);
		return ResponseEntity.ok(stocks);
	}

	@Operation(summary = "Create a new stock")
	@ApiResponses(value = { 
	  @ApiResponse(responseCode = "201", description = "Stock created", 
	    content = { @Content(mediaType = "application/json", 
	      schema = @Schema(implementation = StockDTO.class)) }),
	  @ApiResponse(responseCode = "400", description = "Bad request", 
	    content = @Content), 
	   })
	@PostMapping(value = "v1/stocks",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StockDTO> createStock(@RequestBody StockRequest request) throws StockAlreadyPresentException {
		logger.info("Post :: v1/stocks :: body :: {}", request.toString());
		StockDTO stock = modelMapper.map(request, StockDTO.class);
		logger.info("Post :: v1/stocks :: response object :: {}", stock.toString());
		return ResponseEntity.status(HttpStatus.CREATED).body(iStockService.createStock(stock));
	}

	@Operation(summary = "Get a stock by its Id")
	@ApiResponses(value = { 
	  @ApiResponse(responseCode = "200", description = "Stock is present", 
	    content = { @Content(mediaType = "application/json", 
	      schema = @Schema(implementation = StockDTO.class)) }),
	  @ApiResponse(responseCode = "400", description = "Bad request", 
	    content = @Content), 
	  @ApiResponse(responseCode = "404", description = "Stock not present", 
	    content = @Content) })
	@GetMapping(value = "v1/stocks/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StockDTO> getStockById(@PathVariable(value = "id") Integer id)
			throws RecordNotFoundException {
		logger.info("GET :: v1/stocks/{} :: ", id);
		StockDTO stock = iStockService.getStockById(id);
		logger.info("GET :: v1/stocks/{} :: response object :: {}", id, stock.toString());
		return ResponseEntity.ok(stock);
	}

	@Operation(summary = "Update stock by its Id")
	@ApiResponses(value = { 
	  @ApiResponse(responseCode = "200", description = "Stocks updated", 
	    content = { @Content(mediaType = "application/json", 
	      schema = @Schema(implementation = StockDTO.class)) }),
	  @ApiResponse(responseCode = "400", description = "Bad request", 
	    content = @Content), 
	  @ApiResponse(responseCode = "404", description = "Stock not present", 
	    content = @Content) })
	@PatchMapping(value = "v1/stocks/{id}",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StockDTO> updateStock(@RequestBody StockRequest request,@PathVariable("id") Integer id) throws RecordNotFoundException{
		logger.info("PATCH :: v1/stocks/{} :: ",id);
		StockDTO stock =  modelMapper.map(request,StockDTO.class);
		stock = iStockService.updateStock(id, stock);
		logger.info("PATCH :: v1/stocks/{} :: response object :: {}",id,stock.toString());
		return ResponseEntity.ok(stock);
	}

	@Operation(summary = "Delete stock by its Id")
	@ApiResponses(value = { 
	  @ApiResponse(responseCode = "204", description = "Stock deleted sucessfully", 
	    content = { @Content(mediaType = "application/json") }),
	  @ApiResponse(responseCode = "400", description = "Bad request", 
	    content = @Content), 
			@ApiResponse(responseCode = "404", description = "Stock not present", 
	    content = @Content) })
	@DeleteMapping(value = "v1/stocks/{id}")
	public ResponseEntity deleteStockById(@PathVariable(value = "id") Integer id)
			throws RecordNotFoundException {
		logger.info("DELETE :: v1/stocks/{} :: ",id);
		iStockService.deleteStock(id);		
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

}
