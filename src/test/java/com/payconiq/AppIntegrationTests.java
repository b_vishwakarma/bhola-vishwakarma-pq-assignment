package com.payconiq;

import static io.restassured.RestAssured.given;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;

import com.payconiq.entity.dtos.StockDTO;
import com.payconiq.entity.dtos.StockRequest;
import com.payconiq.exception.ErrorDTO;
import com.payconiq.repository.IStockRepository;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestInstance(Lifecycle.PER_CLASS)
@TestPropertySource("classpath:application-test.properties")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AppIntegrationTests {
	@LocalServerPort
	int port;

	@Autowired
	IStockRepository iStockRepository;

	@Value("${username}")
	public String username;

	@Value("${password}")
	public String password;

	@BeforeAll
	public void setUp() {
		RestAssured.port = port;
		RestAssured.basePath = "/api/v1";
		RestAssured.defaultParser = Parser.JSON;

	}
	//--------------Test basic authentication------------
	
	@Test
	@Order(0)
	public void testBasicAuthentication1() throws InterruptedException {
		Response response = given().auth().basic(username, password).contentType(ContentType.JSON).when().get("/stocks").then().extract().response();
		Assertions.assertEquals(HttpStatus.OK.value(), response.getStatusCode());
	}
	
	@Test
	@Order(0)
	public void testBasicAuthentication2() throws InterruptedException {
		Response response = given().auth().basic(username, "wrongpass").contentType(ContentType.JSON).when().get("/stocks").then().extract().response();
		Assertions.assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getStatusCode());
			
	}
	

	// ------------------Find All Stocks By Pagination----------------------
	@Test
	@Order(1)
	public void getAllStockTest1() throws InterruptedException {
		Response response = given().auth().basic(username, password).contentType(ContentType.JSON).when().get("/stocks").then().extract().response();
		List<StockDTO> stocks = Arrays.asList(response.getBody().as(StockDTO[].class));
		Assertions.assertEquals(HttpStatus.OK.value(), response.getStatusCode());
		Assertions.assertEquals(10, stocks.size());		
	}

	@Test
	@Order(2)
	public void getAllStockPaginationTest2() throws InterruptedException {
		Response response = given().auth().basic(username, password).contentType(ContentType.JSON).when().get("/stocks?pageNo=0&size=5").then().extract()
				.response();
		List<StockDTO> stocks = Arrays.asList(response.getBody().as(StockDTO[].class));
		Assertions.assertEquals(HttpStatus.OK.value(), response.getStatusCode());
		Assertions.assertEquals(5, stocks.size());
	}

	@Test
	@Order(3)
	public void getAllStockPaginationTest3() {
		Response response = given().auth().basic(username, password).contentType(ContentType.JSON).when().get("/stocks?pageNo=1&size=5").then().extract()
				.response();
		List<StockDTO> stocks = Arrays.asList(response.getBody().as(StockDTO[].class));
		Assertions.assertEquals(HttpStatus.OK.value(), response.getStatusCode());
		Assertions.assertEquals(5, stocks.size());
	}

	@Test
	@Order(4)
	public void getAllStockPaginationTest4() {
		Response response = given().auth().basic(username, password).contentType(ContentType.JSON).when().get("/stocks?pageNo=1").then().extract()
				.response();
		List<StockDTO> stocks = Arrays.asList(response.getBody().as(StockDTO[].class));
		Assertions.assertEquals(HttpStatus.OK.value(), response.getStatusCode());
		Assertions.assertEquals(10, stocks.size());
	}

	@Test
	@Order(5)
	public void getAllStockPaginationTest5() {
		Response response = given().auth().basic(username, password).contentType(ContentType.JSON).when().get("/stocks?pageNo=8&size=10").then()
				.extract().response();
		ErrorDTO erorr = response.getBody().as(ErrorDTO.class);
		Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCode());
	}
	
	@Test
	@Order(5)
	public void getAllStockPaginationTest6() {
		// if the input from the user is -ve on page size and page no
		Response response = given().auth().basic(username, password).contentType(ContentType.JSON).when().get("/stocks?pageNo=-8&size=-10").then()
				.extract().response();
		ErrorDTO erorr = response.getBody().as(ErrorDTO.class);
		Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), erorr.status);
		Assertions.assertEquals("Page Number or Page Size should not be less then zero",erorr.errors.get(0));
	}

	// ------------------Create Stock----------------------

	@Test
	@Order(6)
	public void createStockTest1() {
		StockRequest request = new StockRequest();
		request.setCurrentPrice(BigDecimal.valueOf(23.52));
		request.setName("Nivea");
		int id = iStockRepository.getMaxStockId();
		Response response = given().auth().basic(username, password).contentType(ContentType.JSON).body(request).when().post("/stocks").then().extract()
				.response();
		StockDTO stock = response.getBody().as(StockDTO.class);
		Assertions.assertEquals(HttpStatus.CREATED.value(), response.getStatusCode());
		Assertions.assertEquals(++id,stock.getId());
		Assertions.assertEquals("Nivea",stock.getName());
		Assertions.assertEquals(BigDecimal.valueOf(23.52),stock.getCurrentPrice());
		Assertions.assertNotNull(stock.getLastUpdated());
		
	}
	
	@Test
	@Order(7)
	public void createStockTest2() {
		StockRequest request = new StockRequest();
		request.setCurrentPrice(BigDecimal.valueOf(23.52));
		request.setName(null);		
		Response response = given().auth().basic(username, password).contentType(ContentType.JSON).body(request).when().post("/stocks").then().extract()
				.response();
		ErrorDTO error = response.getBody().as(ErrorDTO.class);
		Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatusCode());		
		Assertions.assertEquals("Stock name cannot be blank",error.errors.get(0));
	}
	
	@Test
	@Order(7)
	public void createStockTest3() {
		StockRequest request = new StockRequest();
		request.setCurrentPrice(BigDecimal.valueOf(23.52));
		request.setName("Apple");		
		Response response = given().auth().basic(username, password).contentType(ContentType.JSON).body(request).when().post("/stocks").then().extract()
				.response();
		ErrorDTO error = response.getBody().as(ErrorDTO.class);
		Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatusCode());		
		Assertions.assertEquals("Stock : Apple : already present.",error.errors.get(0));
	}
	
	
	@Test
	@Order(8)
	public void createStockTest4() {
		StockRequest request = new StockRequest();
		request.setCurrentPrice(null);
		request.setName("Bata");		
		Response response = given().auth().basic(username, password).contentType(ContentType.JSON).body(request).when().post("/stocks").then().extract()
				.response();
		ErrorDTO error = response.getBody().as(ErrorDTO.class);
		Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatusCode());		
		Assertions.assertEquals("Stock price cannot be blank",error.errors.get(0));
	}
	
	// ------------------Update Stock----------------------

	@Test
	@Order(9)
	public void updteStockTest1() {
		StockRequest request = new StockRequest();
		request.setCurrentPrice(null);
		request.setName("Bata");
		Response response = given().auth().basic(username, password).contentType(ContentType.JSON).body(request).when().patch("/stocks/1").then().extract()
				.response();
		StockDTO stock = response.getBody().as(StockDTO.class);
		Assertions.assertEquals(HttpStatus.OK.value(), response.getStatusCode());		
		Assertions.assertNotEquals("Apple",stock.getName());
		Assertions.assertEquals("Bata",stock.getName());
	}
	
	@Test
	@Order(10)
	public void updteStockTest2() {
		StockRequest request = new StockRequest();
		request.setCurrentPrice(BigDecimal.valueOf(89.55));		
		
		Response response = given().auth().basic(username, password).contentType(ContentType.JSON).body(request).when().patch("/stocks/3").then().extract()
				.response();
		StockDTO stock = response.getBody().as(StockDTO.class);
		Assertions.assertEquals(HttpStatus.OK.value(), response.getStatusCode());		
		Assertions.assertNotEquals(BigDecimal.valueOf(92.50),stock.getCurrentPrice());
		Assertions.assertEquals(BigDecimal.valueOf(89.55),stock.getCurrentPrice());
		Assertions.assertEquals("Tesla",stock.getName());		
		
	}
	
	// ------------------Delete Stock----------------------
	@Test
	@Order(11)
	public void deleteStockTest1() {		
		Response response = given().auth().basic(username, password).contentType(ContentType.JSON).when().delete("/stocks/1").then().extract()
				.response();
		Assertions.assertEquals(HttpStatus.NO_CONTENT.value(), response.getStatusCode());		
				
		
	}
	
	@Test
	@Order(12)
	public void deleteStockTest2() {		
		Response response = given().auth().basic(username, password).contentType(ContentType.JSON).when().delete("/stocks/85").then().extract()
				.response();
		ErrorDTO error = response.getBody().as(ErrorDTO.class);
		Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCode());
		Assertions.assertEquals("Not a valid stock Id : 85 to be deleted", error.errors.get(0));
	}
	
	// ------------------Find Stock By Id----------------------
	@Test
	@Order(13)
	public void getStockByIdTest1() {		
		Response response = given().auth().basic(username, password).contentType(ContentType.JSON).when().get("/stocks/85").then().extract()
				.response();
		ErrorDTO error = response.getBody().as(ErrorDTO.class);
		Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCode());
		Assertions.assertEquals("No stock present for the given ID : 85", error.errors.get(0));
	}
	
	@Test
	@Order(14)
	public void getStockByIdTest2() {		
		Response response = given().auth().basic(username, password).contentType(ContentType.JSON).when().get("/stocks/7").then().extract()
				.response();
		StockDTO stock = response.getBody().as(StockDTO.class);
		Assertions.assertEquals("Ongc",stock.getName());
		Assertions.assertTrue(BigDecimal.valueOf(53.50).compareTo(stock.getCurrentPrice())==0?true:false);
		Assertions.assertEquals(HttpStatus.OK.value(),response.getStatusCode());		
	}
}


