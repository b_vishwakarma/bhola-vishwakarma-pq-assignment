package com.payconiq;

import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.payconiq.controller.StockController;
import com.payconiq.entity.dtos.StockDTO;
import com.payconiq.entity.dtos.StockRequest;
import com.payconiq.exception.InvalidInputException;
import com.payconiq.exception.RecordNotFoundException;
import com.payconiq.exception.StockAlreadyPresentException;
import com.payconiq.repository.IStockRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestPropertySource("classpath:application-test.properties")
public class AppUnitTests {

	@Autowired
	StockController controller;

	@Autowired
	IStockRepository iStockRepository;

	@Test
	@Order(1)
	public void getAllStockTest1() throws InterruptedException, RecordNotFoundException, InvalidInputException {
		ResponseEntity<List<StockDTO>> stocks = controller.getAllStocks(1, 2);
		Assertions.assertEquals(200, stocks.getStatusCodeValue());
		Assertions.assertEquals(2, stocks.getBody().size());
	}

	@Test
	@Order(2)
	public void getAllStockTest2() throws InterruptedException, RecordNotFoundException, InvalidInputException {
		ResponseEntity<List<StockDTO>> stocks = controller.getAllStocks(0, 5);
		Assertions.assertEquals(200, stocks.getStatusCodeValue());
		Assertions.assertEquals(5, stocks.getBody().size());
	}

	@Test
	@Order(3)
	public void getAllStockTest3() throws InterruptedException, RecordNotFoundException, InvalidInputException {
		InvalidInputException exception = Assertions.assertThrows(InvalidInputException.class, () -> {
			controller.getAllStocks(-8, -5);
		}, "");

		Assertions.assertEquals("Page Number or Page Size should not be less then zero", exception.getMessage());
		
	}

	// -----------------Create Stock----------------------
	@Test
	@Order(4)
	public void createStockTest1() throws StockAlreadyPresentException {
		StockRequest request = new StockRequest();
		request.setCurrentPrice(BigDecimal.valueOf(23.52));
		request.setName("Nivea");
		int id = iStockRepository.getMaxStockId();

		ResponseEntity<StockDTO> stock = controller.createStock(request);

		Assertions.assertEquals(201, stock.getStatusCode().value());
		Assertions.assertEquals(++id, stock.getBody().getId());
		Assertions.assertEquals("Nivea", stock.getBody().getName());
		Assertions.assertEquals(BigDecimal.valueOf(23.52), stock.getBody().getCurrentPrice());
		Assertions.assertNotNull(stock.getBody().getLastUpdated());

	}
	
	@Test
	@Order(4)
	public void createStockTest2() throws StockAlreadyPresentException {
		StockRequest request = new StockRequest();
		request.setCurrentPrice(BigDecimal.valueOf(23.52));
		request.setName("Apple");
		StockAlreadyPresentException exception = Assertions.assertThrows(StockAlreadyPresentException.class, () -> {
			controller.createStock(request);
		}, "");

		Assertions.assertEquals("Stock : Apple : already present.", exception.getMessage());

	}

	// ------------------Update Stock----------------------

	@Test
	@Order(5)
	public void updteStockTest1() throws RecordNotFoundException {
		StockRequest request = new StockRequest();
		request.setCurrentPrice(null);
		request.setName("Bata");
		ResponseEntity<StockDTO> stock = controller.updateStock(request, 1);
		Assertions.assertEquals(200, stock.getStatusCode().value());
		Assertions.assertNotEquals("Apple", stock.getBody().getName());
		Assertions.assertEquals("Bata", stock.getBody().getName());
	}

	@Test
	@Order(6)
	public void updteStockTest2() throws RecordNotFoundException {
		StockRequest request = new StockRequest();
		request.setCurrentPrice(BigDecimal.valueOf(89.55));
		ResponseEntity<StockDTO> stock = controller.updateStock(request, 3);
		Assertions.assertEquals(200, stock.getStatusCode().value());
		Assertions.assertNotEquals(BigDecimal.valueOf(92.50), stock.getBody().getCurrentPrice());
		Assertions.assertEquals(BigDecimal.valueOf(89.55), stock.getBody().getCurrentPrice());
		Assertions.assertEquals("Tesla", stock.getBody().getName());
	}
	
	@Test
	@Order(6)
	public void getStockByIdTest3() throws RecordNotFoundException {
		StockRequest request = new StockRequest();
		request.setCurrentPrice(BigDecimal.valueOf(89.55));
		RecordNotFoundException exception = Assertions.assertThrows(RecordNotFoundException.class, () -> {
			controller.updateStock(request,74);
		}, "");

		Assertions.assertEquals("No stock present for the given ID : 74", exception.getMessage());

	}

	// ------------------Delete Stock----------------------

	@Test
	@Order(7)
	public void deleteStockTest1() throws RecordNotFoundException {
		ResponseEntity response = controller.deleteStockById(3);
		Assertions.assertEquals(204, response.getStatusCodeValue());
	}
	
	@Test
	@Order(8)
	public void deleteStockTest2() throws RecordNotFoundException {
		RecordNotFoundException exception = Assertions.assertThrows(RecordNotFoundException.class, () -> {
			controller.deleteStockById(85);
		}, "");

		Assertions.assertEquals("Not a valid stock Id : 85 to be deleted", exception.getMessage());

	}

	// ------------------Find Stock By Id----------------------
	@Test
	@Order(9)
	public void getStockByIdTest1() throws RecordNotFoundException {
		RecordNotFoundException exception = Assertions.assertThrows(RecordNotFoundException.class, () -> {
			controller.getStockById(85);
		}, "");

		Assertions.assertEquals("No stock present for the given ID : 85", exception.getMessage());

	}
	
	@Test
	@Order(10)
	public void getStockByIdTest2() throws RecordNotFoundException {
		ResponseEntity<StockDTO> response = controller.getStockById(7);
		Assertions.assertEquals("Ongc",response.getBody().getName());
		Assertions.assertTrue(BigDecimal.valueOf(53.50).compareTo(response.getBody().getCurrentPrice())==0?true:false);

	}

}
