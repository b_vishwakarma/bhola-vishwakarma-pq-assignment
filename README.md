# Rest endpoints to manage stock prices. 

---

## How to run the app with docker image ?. 

 1. Assuming docker client is running on your machine.
 2. Open up a terminal and execute the command ** sudo docker pull bholavishwakarma/stock-info:latest **. This will download the docker image.
 3. To start the app, execute the command ** sudo docker run -p 8080:8080 bholavishwakarma/stock-info **. 
 4. In the console wait for the message ** Started AppRunner **
 
## How to test the app ?
 
 1. Open up any browser and browse for ** http://localhost:8080/swagger-ui/index.html **.
 2. You will be prompted for authentication. Use ** username = payconiq , password = payconiq **.
 3. Swagger UI will be up and running to test the endpoints. 


## Rest endpoints available to manage the stocks. 

 1. GET    :: /api/v1/stocks/ ** To get all the stocks. This endpoint can page through the records by providing the page number & page size **.
 2. GET    :: /api/v1/stocks/{id} ** To get a stock by its Id **.
 3. POST   :: /api/v1/stocks/ ** To create a new stock **.
 4. PATCH  :: /api/v1/stocks/{id} ** To update a stock price or name **.
 5. DELETE :: /api/v1/stocks/{id} ** To delete a stock **.
 

## Technology Stack

 1. Java 11.
 2. Spring Boot 2.6.3.
 3. Basic Spring Security for the rest endpoints.
 3. H2 database. An application can be configured to use mysql or any other RDBMS database with config change.
 4. Open Api 1.6.4 for documentation of reset endpoints. 
 5. Rest Assured library for integration testing.
 6. Junit  5  for unit testing.
 7. Tomact 9  as a deployment container.
 8. Maven as a build tool.
 